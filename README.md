# AQS Docker Test Environment

This is a Dockerized, single-node Cassandra test environment. It includes tooling to create a schema and load a small sample set for AQS endpoints. 

## Developer Guide: Getting Started

Each AQS service serves data from a Cassandra production cluster. This testing environment is meant to emulate the Cassandra production datastore. 

Each Go service is run from its own repository:

[Unique Devices](https://gerrit.wikimedia.org/g/generated-data-platform/aqs/device-analytics)

Follow the guide in each microservice's individual repository to start it up. To serve local test data, run this Cassandra test environment.

### Build and Run

You will need:
- Go
- Make
- Docker Compose


```sh-session
make startup
```

Wait until you see the 'Startup complete' log message, then in another terminal, bootstrap the database schema and sample data:

```sh-session
make bootstrap
```

### Troubleshooting

Other applications, notably the mwcli mediawiki local development tool, may also use this port. If the port is unavailable, shut it down in pageviews before starting this container:

In /pageviews:

```sh-session
make -C docker shutdown
```

## Sample data

### Pageviews

Pageviews per article counts the number of pageviews for a given article. The included sample dataset contains data from July 1, 2015 to August 17, 2021 for article title "Banana".
 
This same dataset is duplicated with the fake article title "Mock: -/%" for testing url-encoded characters.

Pageviews per project counts the number of pageviews on a given project by access type and agent. The included sample dataset contains data from Jan 1, 2021 through Jan 1, 2022. 

### Unique Devices

Unique devices counts the number of unique device hits on a given project by access site. The included sample dataset contains data from January 1, 2020 to March 1 2023. 

### Mediarequests

Mediarequests per file (local_group_default_T_mediarequest_per_file) counts the number of requests for a given file. The included sample dataset contains data from the year 2020 for filepath end%252Fwikipedia%252Fcommons%252F1%252F1c%252FManhattan_Bridge_Construction_1909.jpg 

Top mediarequests counts the most-viewed media properties for a given day by referer and media type. The included sample dataset contains data from January 1, 2020.

Mediarequests per referer counts the total number of mediarequests for a given day by referer. The included sample dataset contains data from 20200101 to 20200102

### Editors

Only one editors endpoint is hosted on AQS, editors/by-country. The included sample dataset contains data from Oct-Dec of 2020 for the three most-viewed wikis, fr, de, en .wikis.

## Updating the included data sets

### Running the included scripts

A developer might have reason to update the data included for testing. This repository includes scripts that a developer with the appropriate access can modify and run in order to pull data from the production cluster. 

In order to follow this guide, you will need access to the AQS host and to the Cassandra cluster. 

The included script pulls data for the unique devices endpoint and transforms it to comma-delimited data. Once you have attained access, you can modify and run included scripts on the Cassandra cluster in order to copy and transform production data.

1. Copy the desired script to the target AQS server. For example, to generate a new csv file for the unique devices dataset, replacing "user" with your username on the AQS host,

    ```console
    user@local:~/location/of/aqs-docker-test-env$ scp /scripts/local_group_default_T_unique_devices.sh aqs1010.eqiad.wmnet:/home/user/
    ```


2. ssh in to the server-- In this guide, we use AQS1010.

    ```ssh aqs1010.eqiad.wmnet```

    create cqlshrc file in the .cassandra directory on your home directory on the server, 

    ```cd .cassandra```

    ```touch cqlshrc```

    ***IMPORTANT*** change permissions so that only you can read or write this file,

    ```chmod 400 cqlshrc```

    and open the file.

    ```vim cqlshrc```

3. Add your credentials and the desired Cassandra cluster to connect to, in this format.

    ```
    [authentication]
    username = YOUR USERNAME
    password = YOUR PASSWORD

    [connection]
    hostname = aqs1010-a.eqiad.wmnet
    ```

4. run the script in your home directory on the host.

    ```./local_group_default_T_unique_devices.sh```

    If all goes according to plan, you'll have a file named expected_filename.csv in your home directory on the target AQS server.


4. Copy this file to the desired location on your home machine. You can copy it directly into the /test_data.d folder in the test environment. Ensure that the name of the file follows the format dictated in the schema: local_group_default_T... For example,

    ```console
    user@local:~/location/of/aqs-docker-test-env$ scp aqs1010.eqiad.wmnet:/home/user/local_group_default_T_unique_devices.csv test_data.d
    ```


5. From here, you should be able to startup and boot the test environment completely as expected (See: Build and Run, above).

6. Once you've successfully updated the necessary test data, remove the cqlshrc file from your home directory on the AQS server.
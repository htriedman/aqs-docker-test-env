#!/bin/bash

# Legacy pagecounts query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access_sites="('all-sites', 'desktop-site', 'mobile-site')"
granularities="('monthly', 'daily', 'hourly')"
start="2009010101"
stop="2008120101"
output_filename="local_group_default_T_lgc_pagecounts_per_project.csv"

printf "select * from \"local_group_default_T_lgc_pagecounts_per_project\".data\
 where \"_domain\" = '$domain' and project = '$project'\
 and \"access-site\" in $access_sites and granularity in $granularities and timestamp < '$start' and timestamp > '$stop';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
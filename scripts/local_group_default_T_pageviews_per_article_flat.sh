#!/bin/bash

#Pageviews per article query
domain="analytics.wikimedia.org"
project="en.wikipedia"
article="Banana"
granularities="('monthly', 'daily', 'hourly')"
start="20150701"
stop="20210817"
output_filename="local_group_default_T_pageviews_per_article_flat.csv"

printf "select * from \"local_group_default_T_pageviews_per_article_flat\".data\
 where \"_domain\" = '$domain' AND project = '$project' AND article = '$article'
  AND granularity in $granularities AND timestamp >= '$start' AND timestamp <= '$stop';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
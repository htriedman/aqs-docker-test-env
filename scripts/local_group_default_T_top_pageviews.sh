#!/bin/bash

#Top by country query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access="('all-access', 'desktop', 'mobile-app', 'mobile-web')"
year="2021"
month="01"
output_filename="local_group_default_T_top_pageviews.csv"

# declare empty "day" array
day=()
# fill with dates
for i in {1..31}; do
 varToAdd=\'${i}\'
 day+=($varToAdd)
done

# convert to string
IFS=", "
dayString="(${day[*]})"

printf "SELECT * FROM \"local_group_default_T_top_pageviews\".data
 where \"_domain\" = '$domain' and project = '$project' and 
 access in $access and year = '$year' and month = '$month' and day in $dayString;"\
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
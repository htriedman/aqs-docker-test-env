#!/bin/bash

#editors by country query 
domain="analytics.wikimedia.org"
project="('de.wikipedia', 'fr.wikipedia', 'en.wikipedia')"
activity_level="('5..99-edits', '100..-edits')"
year="2020"
output_filename="local_group_default_T_editors_bycountry.csv"

month=()
#fill empty "month" array with values to represent months
for i in {1..12}; do
    addMonth=\'${i}\'
    month+=($addMonth)
done

IFS=", "
monthString="${month[*]}"

printf "select * from \"local_group_default_T_editors_bycountry\".data\
 where \"_domain\" = '$domain' and project in $project and \"activity-level\" in $activity_level\
  and year = '$year' and month in ($monthString);"\
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
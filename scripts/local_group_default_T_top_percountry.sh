#!/bin/bash

# Top per country query
domain="analytics.wikimedia.org"
countryCode="GB"
access_sites="('all-access', 'desktop', 'mobile-app', 'mobile-web')"
year="2021"
month="01"
day=()
output_filename="local_group_default_T_top_percountry.csv"

# declare empty "day" array
day=()
# fill with dates
for i in {1..31}; do
 varToAdd=\'${i}\'
 day+=($varToAdd)
done

# convert to string
IFS=", "
dayString="${day[*]}"

printf "select * from \"local_group_default_T_top_percountry\".data\
 where \"_domain\" = '$domain' and country = '$countryCode'\
 and access in $access_sites and year = '$year' and  month = '$month' and day in ($dayString);"\
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
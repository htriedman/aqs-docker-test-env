#!/bin/bash

#mediarequest per referer query
domain="analytics.wikimedia.org"
referer="('en.wikipedia', 'all-referers', 'internal', 'external', 'search-engine', 'unknown', 'none')"
media_type="('all-media-types', 'audio', 'video', 'document', 'other')"
agent="('all-agents', 'spider', 'user')"
granularity="('daily', 'monthly')"
start="20200101"
stop="20200201"
output_filename="local_group_default_T_mediarequest_per_referer.csv"

printf "select * from \"local_group_default_T_mediarequest_per_referer\".data\
 where \"_domain\" = '$domain' and referer in $referer and media_type in $media_type and agent in $agent and granularity in $granularity and timestamp > '$start' and timestamp < '$stop';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
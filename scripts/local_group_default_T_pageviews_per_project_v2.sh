#!/bin/bash

#Pageviews per project query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access="('all-access', 'desktop', 'mobile-app', 'mobile-web')"
agent="('all-agents', 'automated', 'spider', 'user')"
granularity="('monthly', 'daily', 'hourly')"
start="20210101"
stop="20220101"
output_filename="local_group_default_T_pageviews_per_project_v2.csv"


printf "select * from \"local_group_default_T_pageviews_per_project_v2\".data\
 where \"_domain\" = '$domain' and project = '$project' and access in $access
  and agent in $agent and granularity in $granularity and timestamp >= '$start' and timestamp < '$stop';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
#!/bin/bash

# mediarequest per file query
domain="analytics.wikimedia.org"
referer="('en.wikipedia', 'all-referers', 'internal', 'external', 'search-engine', 'unknown', 'none')"
file_path="/wikipedia/commons/1/1c/Manhattan_Bridge_Construction_1909.jpg"
granularity="('daily', 'monthly')"
start="20200101"
stop="20210101"
output_filename="local_group_default_T_mediarequest_per_file.csv"

printf "select * from \"local_group_default_T_mediarequest_per_file\".data 
where \"_domain\" = '$domain' and referer in $referer and file_path = '$file_path' 
and granularity in $granularity and timestamp > '$start' and timestamp < '$stop';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
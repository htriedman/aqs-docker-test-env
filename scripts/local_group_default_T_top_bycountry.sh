#!/bin/bash

#Top by country query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access="('all-access', 'desktop', 'mobile-app', 'mobile-web')"
year="2020"
output_filename="local_group_default_T_top_bycountry.csv"

month=()
#fill empty "month" array with values to represent months
for i in {1..12}; do
    addMonth=\'${i}\'
    month+=($addMonth)
done

# convert to string
IFS=", "
monthString="(${month[*]})"

printf "select * from \"local_group_default_T_top_bycountry\".data
 where \"_domain\" = '$domain' and project = '$project' and access in $access
  and year = '$year' and month in $monthString;"\
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
#!/bin/bash

#top mediarequests query
domain="analytics.wikimedia.org"
referer="('en.wikipedia', 'all-referers', 'internal', 'external', 'search-engine', 'unknown', 'none')"
media_type="('all-media-types', 'audio', 'video', 'document', 'other')"
year="2020"
month="01"
day="01"
output_filename="local_group_default_T_mediarequest_top_files.csv"

printf "select * from \"local_group_default_T_mediarequest_top_files\".data
 where \"_domain\" = '$domain' and referer in $referer and media_type in $media_type
  and year = '$year' and month = '$month' and day = '$day';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename
#!/bin/bash

# Unique devices query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access_sites="('all-sites', 'desktop-site', 'mobile-site')"
granularities="('monthly', 'daily')"
output_filename="local_group_default_T_unique_devices.csv"

printf "select * from \"local_group_default_T_unique_devices\".data\
 where \"_domain\" = '$domain' and project = '$project'\
 and \"access-site\" in $access_sites and granularity in $granularities and timestamp > '20200101';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
> $output_filename